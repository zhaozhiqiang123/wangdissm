<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <%@include file="../../common/head.jsp" %>
</head>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">

    <%@include file="../../common/header.jsp" %>

    <div class="main-container">

        <%@include file="../../common/sidebar.jsp" %>

        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header text-white bg-primary">
                            客户管理
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>编号</th>
                                        <th>姓名</th>
                                        <th>邮箱</th>
                                        <th>电话</th>
                                        <th>地址</th>
                                        <th>权限</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${pageInfo.list}" var="users">
                                        <tr>
                                            <td>${users.id}</td>
                                            <td>${users.username}</td>
                                            <td>${users.email}</td>
                                            <td>${users.mobile}</td>
                                            <td>${users.address}</td>
                                            <td>${users.roleName}</td>

                                            <td>
                                                <button type="button" class="btn btn-sm btn-success" onclick="window.location.href='/users/update?id=${users.id}'">更 新</button>
                                                <button type="button" class="btn btn-sm btn-danger" onclick="deleteCustomer(${users.id})">删 除</button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>

                            <!-- 提示分页信息行 -->
                            <%@include file="../../common/pages.jsp" %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="../../common/message.jsp" %>
<%@include file="../../common/js.jsp" %>
<script>
    function deleteCustomer(id) {
        $('#deleteModal').modal('show');
        $("#deleteButton").on("click", function () {
            $.ajax({
                type: 'GET',
                url: '/users/delete?id=' + id,
                dataType: "json",
                success: function (data) {
                    console.log("进入方法1111")
                    $('#deleteModal').modal('hide');
                    if (data.code == 200) {
                        window.location.href = "/users/userlist?pageNumber=${pageInfo.pageNum}";
                    } else {
                        $('#modal-danger').modal('show');
                        $('#modal-danger .modal-body').html("失败：状态码：" + data.code + "，" + data.msg);
                    }
                },
                error: function () {
                    $('#deleteModal').modal('hide');
                    $('#modal-danger').modal('show');
                    $('#modal-danger .modal-body').html('请求失败，请检查请求数据或网络哟');
                }
            });
        });
    }
</script>
</body>
</html>
