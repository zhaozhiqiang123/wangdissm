package test;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Assert;
import org.junit.Test;

public class LoginLogoutTest {

    @Test
    public void testHelloworld() {
//1、获取 SecurityManager 工厂，此处使用 Ini 配置文件初始化 SecurityManager
        Factory<SecurityManager> factory =
                new IniSecurityManagerFactory("classpath:shiro-password.ini");
//2、得到 SecurityManager 实例 并绑定给 SecurityUtils
        SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);
//3、得到 Subject 及创建用户名/密码身份验证 Token（即用户身份/凭证）
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("wang", "123");
        try {
//4、登录，即身份验证
            subject.login(token);
        } catch (AuthenticationException e) {
//5、身份验证失败
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getMessage());
            System.out.println("身份验证失败");
        }
        boolean authenticated = subject.isAuthenticated();
        System.out.println("最终的结果是 ： " + authenticated);

        Assert.assertEquals(true, subject.isAuthenticated()); //断言用户已经登录
//6、退出
        subject.logout();
    }

  @Test
    public void test02(){
        String algorithmName = "md5";
        String username = "wang";
        String password = "123";
        String salt1 = username;
        String salt2 = new SecureRandomNumberGenerator().nextBytes().toHex();
        int hashIterations = 2;
        SimpleHash hash = new SimpleHash(algorithmName, password, salt1 + salt2, hashIterations);
      System.out.println(salt2);
      System.out.println(hash);
    }











}
