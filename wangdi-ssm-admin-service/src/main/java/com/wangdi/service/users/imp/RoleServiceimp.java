package com.wangdi.service.users.imp;


import com.wangdi.service.users.RoleService;
import com.wangdi.ssm.domain.users.Roles;
import com.wangdi.ssm.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceimp implements RoleService {
    @Autowired
    private RoleMapper roleMapper;
    @Override
    public Roles createRole(Roles role) {
        return null;
    }

    @Override
    public void deleteRole(Long roleId) {

    }

    @Override
    public void correlationPermissions(Long roleId, Long... permissionIds) {

    }

    @Override
    public void uncorrelationPermissions(Long roleId, Long... permissionIds) {

    }

    @Override
    public List<Roles> rolesList() {

        return roleMapper.roleList();
    }

    @Override
    public void uidAndrid(Integer uid, Integer rid) {
        roleMapper.ridAnduidAdd(uid,rid);
    }
}