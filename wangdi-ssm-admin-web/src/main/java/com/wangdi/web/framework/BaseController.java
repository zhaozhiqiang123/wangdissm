package com.wangdi.web.framework;

import com.wangdi.web.servieutils.CWMUtils;
import org.springframework.beans.factory.annotation.Autowired;
@SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
public class BaseController {

    @Autowired
    protected CWMUtils cwmUtils;
}
