package com.wangdi.web.model;

public class ResponseCode {

    private Integer code;
    private String msg;
    private Object data;
    private String defaultUrl;

    public ResponseCode(Integer code, String msg, Object data, String defaultUrl) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.defaultUrl = defaultUrl;
    }

    public ResponseCode() {
    }

    public String getDefaultUrl() {
        return defaultUrl;
    }

    public void setDefaultUrl(String defaultUrl) {
        this.defaultUrl = defaultUrl;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
