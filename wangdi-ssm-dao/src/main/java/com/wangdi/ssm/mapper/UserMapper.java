package com.wangdi.ssm.mapper;

import com.github.pagehelper.Page;
import com.wangdi.ssm.domain.users.Users;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface UserMapper {

    public Users userfindAll();
    @Select("select * from users where username=#{username}")
     public   List<Users> findByUsername(String username);
    @Select("select * from users where isdelete=0")
    public Page<Users> UserfindAll2();


    int createUser(Users user);
    @Select("select * from users where id=#{id}")
    Users userfindone(Integer id);

    void update(Users users);


}
