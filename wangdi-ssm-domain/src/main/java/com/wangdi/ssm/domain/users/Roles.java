package com.wangdi.ssm.domain.users;

public class Roles {

    private Integer id;
    /*
     *角色
     * */
    private String role;
    /*
    * 角色描述
    * */
    private String description;
    /*
    * 是否可用
    * */
    private String  available;
    private Integer isdelete=0;
    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public Integer getId() {
        return id;
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocked() {
        return locked;
    }


    public void setRole(String role) {
        this.role = role;
    }



    public void setLocked(String locked) {
        this.locked = locked;
    }

    private String locked;




}
