package com.wangdi.service.users;


import com.wangdi.ssm.domain.users.Roles;

import java.util.List;

public interface RoleService {
    public Roles createRole(Roles role);
    public void deleteRole(Long roleId);
    //添加角色-权限之间关系
    public void correlationPermissions(Long roleId, Long... permissionIds);
    //移除角色-权限之间关系
    public void uncorrelationPermissions(Long roleId, Long... permissionIds);//

    List<Roles> rolesList();

    public void uidAndrid(Integer uid,Integer rid);
}