package com.wangdi.service.users;

import com.wangdi.ssm.domain.users.Permissions;


public interface PermissionService {
    public Permissions createPermission(Permissions permission);
    public void deletePermission(Long permissionId);
}
