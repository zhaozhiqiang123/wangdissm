package com.wangdi.ssm.utils;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.Test;

public class MDFive {

    public String salt;
    public String password;

    public MDFive(String password,String username) {

        String salt2 = new SecureRandomNumberGenerator().nextBytes().toHex();
        String salt = username + salt2;
        int hashIterations = 2;
        SimpleHash hash = new SimpleHash("md5", password, salt, hashIterations);

        this.password=hash.toString();
        this.salt = salt2;

    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
