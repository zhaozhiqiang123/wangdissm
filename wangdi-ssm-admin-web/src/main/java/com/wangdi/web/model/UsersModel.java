package com.wangdi.web.model;

import java.util.ArrayList;
import java.util.List;

public class UsersModel {

    private Integer id;

    /*
     *uid
     */

    private String uid = "";
    private String username="";

    private String salt ="";
    /*
     *真实姓名
     */

    private String realName = "";
    /*
     *电话
     */

    private String mobile = "";
    /*
     * 邮箱
     * */
    private String email="";
    /*
     *密码
     */

    private String password = "";
    /*
     *用户昵称
     */

    private String nickName = "";

    /*
     *用户权限
     */

    private String roleName = "";
    /*
     *保护地类型权限
     */

    private String protectedTyperole = "";


    private String address = "";

    /**
     * 是否删除
     **/

    private Integer isdelete=0;


    private String county="";


    private String countys="";
    private String rolelist ="";


    public UsersModel() {
    }

    public UsersModel(String username, String email, String address, String rolelist) {
        this.username = username;
        this.email = email;
        this.address = address;
        this.rolelist = rolelist;
    }

    public UsersModel(Integer id, String uid, String username, String salt, String realName, String mobile, String email, String password, String nickName, String roleName, String protectedTyperole, String address, Integer isdelete, String county, String countys, String rolelist) {
        this.id = id;
        this.uid = uid;
        this.username = username;
        this.salt = salt;
        this.realName = realName;
        this.mobile = mobile;
        this.email = email;
        this.password = password;
        this.nickName = nickName;
        this.roleName = roleName;
        this.protectedTyperole = protectedTyperole;
        this.address = address;
        this.isdelete = isdelete;
        this.county = county;
        this.countys = countys;
        this.rolelist = rolelist;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getProtectedTyperole() {
        return protectedTyperole;
    }

    public void setProtectedTyperole(String protectedTyperole) {
        this.protectedTyperole = protectedTyperole;
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCountys() {
        return countys;
    }

    public void setCountys(String countys) {
        this.countys = countys;
    }

    public String getRolelist() {
        return rolelist;
    }

    public void setRolelist(String rolelist) {
        this.rolelist = rolelist;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}