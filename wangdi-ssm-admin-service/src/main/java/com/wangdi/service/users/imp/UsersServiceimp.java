package com.wangdi.service.users.imp;

import com.github.pagehelper.Page;
import com.wangdi.service.users.UsersService;
import com.wangdi.ssm.domain.users.UserandRoles;
import com.wangdi.ssm.domain.users.Users;

import com.wangdi.ssm.mapper.UserAndroleMapper;
import com.wangdi.ssm.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

@Service
public class UsersServiceimp implements UsersService {

     @Resource
    private UserMapper usersDao;
   @Autowired
   private UserAndroleMapper userAndroleMapper;
    @Override
    public Page<Users> userFindAll2() {
      Page<Users> users = usersDao.UserfindAll2();
        return users;
    }

  @Override
  public int createUser(Users user) {

    return usersDao.createUser(user);

  }

  @Override
  public void changePassword(Long userId, String newPassword) {

  }

  @Override
  public void correlationRoles(Long userId, Long... roleIds) {

  }

  @Override
  public void uncorrelationRoles(Long userId, Long... roleIds) {

  }

  @Override
  public    List<Users> findByUsername(String username) {

    List<Users> byUsername = usersDao.findByUsername(username);

    return byUsername;
  }

  @Override
  public Set<String> findRoles(String username) {
    return null;
  }

  @Override
  public Set<String> findPermissions(String username) {
    return null;
  }

  @Override
  public Users userFindOne(Integer id) {

     return usersDao.userfindone(id);
  }

  @Override
  public void update(Users users) {
     usersDao.update(users);
  }

  @Override
  public void delete(Users users) {
    users.setIsdelete(1);
    usersDao.update(users);
  }

  @Override
  public void creatUidAndRid(UserandRoles userandRoles) {
    userAndroleMapper.creatUidAndRid( userandRoles);
  }

  @Override
  public void roleUpdates(Integer id, List<Integer> roleIds) {

       userAndroleMapper.deleterids(id);
       for(Integer rid : roleIds){
         UserandRoles userandRoles = new UserandRoles();
         userandRoles.setUid(id);
         userandRoles.setRid(rid);
         userAndroleMapper.creatUidAndRid(userandRoles);
       }


  }
}
