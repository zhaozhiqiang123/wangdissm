package com.wangdi.service.users;

import com.github.pagehelper.Page;
import com.wangdi.ssm.domain.users.UserandRoles;
import com.wangdi.ssm.domain.users.Users;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

public interface UsersService {


    Page<Users>  userFindAll2(); //查询所有用户

    public int createUser(Users user); //创建账户
    public void changePassword(Long userId, String newPassword);//修改密码
    public void correlationRoles(Long userId, Long... roleIds); //添加用户-角色关系
    public void uncorrelationRoles(Long userId, Long... roleIds);// 移除用户-角色关系
    public    List<Users> findByUsername(String username);// 根据用户名查找用户
    public Set<String> findRoles(String username);// 根据用户名查找其角色
    public Set<String> findPermissions(String username); //根据用户名查找其权限

    Users userFindOne(Integer id);

    void update(Users users);

    void delete(Users users);

    void creatUidAndRid(UserandRoles userandRoles);

    void roleUpdates(Integer id, List<Integer> roleIds);

}
