package com.wangdi.web.servieutils;

import com.wangdi.common.helper.WebHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service(value = "CWMUtils")
public class CWMUtils {
    @Autowired
    private HttpServletRequest request;

    /**
     * 设置后台访问referer
     *
     * @param url url地址
     */
    public void setAdminRefererCookie(HttpServletResponse response, String url) {
        WebHelper.setCookie(response, "adminreferer", WebHelper.urlEncode(url));
    }
    /**
     * 获取
     */
    public String getRawUrl() {
        return WebHelper.getRawUrl(request);
    }


    /**
     * 获得后台访问referer
     */
    public String getAdminRefererCookie() {
        return WebHelper.urlDecode(getAdminRefererCookie("/admin/runinfo.html"));
    }

    /**
     * 获得后台访问referer
     *
     * @param defaultUrl 默认地址
     */
    public String getAdminRefererCookie(String defaultUrl) {
        String adminReferer = defaultUrl;
        try {
            adminReferer = WebHelper.urlDecode(WebHelper.getCookieValue(request, "adminreferer"));
            if (adminReferer.length() == 0) {
                adminReferer = defaultUrl;
            }
        } catch (Exception ignored) {
        }

        return adminReferer;
    }
}
