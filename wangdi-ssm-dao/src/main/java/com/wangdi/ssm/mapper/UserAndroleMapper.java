package com.wangdi.ssm.mapper;

import com.github.pagehelper.Page;
import com.wangdi.ssm.domain.users.UserandRoles;
import com.wangdi.ssm.domain.users.Users;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface UserAndroleMapper {


    void creatUidAndRid(UserandRoles userandRoles);
    @Select("select roleid from uid_rid where userid=#{userid}")
    List<Integer> uidfinall(Integer userid);

    @Delete("delete FROM uid_rid where userid=#{userid}")
    void deleterids(Integer userid);

}
