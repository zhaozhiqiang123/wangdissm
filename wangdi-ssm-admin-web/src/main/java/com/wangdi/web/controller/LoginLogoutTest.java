package com.wangdi.web.controller;

import com.alibaba.druid.support.json.JSONUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginLogoutTest {

   /* @RequestMapping(value = "login", method = RequestMethod.POST)*/
    public String testHelloworld(String account, String password, HttpServletRequest request) {


        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(account, password);
        try {
//4、登录，即身份验证
            subject.login(token);
          /*  //登录成功之后跳转到原url
            String url = WebUtils.getSavedRequest(request).getRequestUrl();
*/
            return "pages/index";
        } catch (AuthenticationException e) {
//5、身份验证失败
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getMessage());
            System.out.println("身份验证失败");
        }
        boolean authenticated = subject.isAuthenticated();
        System.out.println("最终的结果是 ： " + authenticated);

        Assert.assertEquals(true, subject.isAuthenticated()); //断言用户已经登录
//6、退出

        return "pages/index";
    }

    @Test
    public void test02() {
        String algorithmName = "md5";
        String username = "wangdi";
        String password = "1234567";
        String salt1 = username;
        String salt2 = new SecureRandomNumberGenerator().nextBytes().toHex();
        int hashIterations = 2;
        SimpleHash hash = new SimpleHash(algorithmName, password, salt1 + salt2, hashIterations);
        System.out.println(salt2);
        System.out.println(hash);
    }


}
