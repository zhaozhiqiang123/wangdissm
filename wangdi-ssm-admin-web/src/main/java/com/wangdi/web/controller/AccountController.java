package com.wangdi.web.controller;

import com.wangdi.web.model.ResponseCode;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.junit.Assert;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


/**
 * 账户关信息
 * @author cqnews
 */
@Controller
public class AccountController  {



    /**
     * 用户登录
     *
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginGet() {

        return "login";
    }

    @RequestMapping(value = "/indexs", method = RequestMethod.GET)
    public String indexsGet() {

        return "index";
    }



    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String testHelloworld(String account, String password, HttpServletRequest request) {


        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(account, password);
        try {
//4、登录，即身份验证
            subject.login(token);
          /*  //登录成功之后跳转到原url
            String url = WebUtils.getSavedRequest(request).getRequestUrl();
*/
            return "index";
        } catch (AuthenticationException e) {
//5、身份验证失败
            System.out.println(e.getLocalizedMessage());
            System.out.println(e.getMessage());
            System.out.println("身份验证失败");
        }
        boolean authenticated = subject.isAuthenticated();
        System.out.println("最终的结果是 ： " + authenticated);

        Assert.assertEquals(true, subject.isAuthenticated()); //断言用户已经登录
//6、退出

        return "index";
    }








/**
     * 退出用户登录
     *
     * @return
     */

    @RequestMapping(value = "/logout",method = RequestMethod.GET)
    @ResponseBody
    public ResponseCode logout() {


        // 获取主体
        Subject subject = SecurityUtils.getSubject();

        subject.logout();

        return new ResponseCode(200,"退出成功",null,null);

    }





}
