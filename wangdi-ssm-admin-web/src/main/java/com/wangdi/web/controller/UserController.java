package com.wangdi.web.controller;



import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wangdi.service.users.RoleService;
import com.wangdi.service.users.UsersService;
import com.wangdi.ssm.domain.users.Roles;
import com.wangdi.ssm.domain.users.UserandRoles;
import com.wangdi.ssm.domain.users.Users;
import com.wangdi.ssm.mapper.UserAndroleMapper;
import com.wangdi.ssm.utils.JsonUtils;
import com.wangdi.ssm.utils.MDFive;
import com.wangdi.web.framework.BaseController;
import com.wangdi.web.model.ResponseCode;
import com.wangdi.web.model.SpinnerModel;
import com.wangdi.web.model.UsersModel;
import net.sf.json.JSONArray;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "users" )
public class UserController extends BaseController {
    @Autowired
    public UsersService usersService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserAndroleMapper userAndroleMapper;
     @RequestMapping(value = "userlist" )
    public String userlist(@RequestParam(defaultValue = "10") Integer pageSize,
                           @RequestParam(defaultValue = "0") Integer pageNumber, Model model,HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {


         cwmUtils.setAdminRefererCookie(response, MessageFormat.format(
                 "{0}?pageSize={1}&pageNumber={2}",
                 cwmUtils.getRawUrl(),
                 pageSize,
                 pageNumber

         ));

         PageHelper.startPage(pageNumber, pageSize);
         List<Users> users = usersService.userFindAll2();
         model.addAttribute("pageInfo",new PageInfo<>(users));
         model.addAttribute("pageurl", cwmUtils.getRawUrl());
         return "users/list";

    }


    @RequestMapping(value = "userinsert", method = RequestMethod.POST )
    @ResponseBody
    public ResponseCode userinsert2(UsersModel usersModel, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Users> byUsername = usersService.findByUsername(usersModel.getUsername());

            if(byUsername.size() >0){
                return new ResponseCode(300,"username不能重复...",null,null);

            }
            String password = usersModel.getPassword();
            String username = usersModel.getUsername();
            String roleName = "";
            List<Integer> roleIds = new ArrayList<>();
            String rolelist = usersModel.getRolelist();
            Object[] objectArray4Json = JsonUtils.getObjectArray4Json(rolelist);
            if(objectArray4Json.length > 0) {
                for (Object o : objectArray4Json) {

                    String[] split = o.toString().split(",");
                    roleIds.add(Integer.decode(split[0]));
                    roleName += split[1] + " ";

                }
            }

           MDFive mdFive = new MDFive(password, username);

            Users users = new Users();
            users.setSalt(mdFive.getSalt());
            users.setPassword(mdFive.getPassword());
            users.setRealName(usersModel.getRealName());
            users.setIsdelete(0);
            users.setEmail(usersModel.getEmail());
            users.setAddress(usersModel.getAddress());
            users.setMobile(usersModel.getMobile());
            users.setUsername(usersModel.getUsername());
          /*  users.setNickName(usersModel.getNickName());*/
            users.setRoleName(roleName);

            int userid = usersService.createUser(users);
            Integer id = users.getId();

            if(roleIds.size() > 0) {
                for (Integer rid : roleIds) {
                    UserandRoles userandRoles = new UserandRoles();
                    userandRoles.setRid(rid);
                    userandRoles.setUid(id);
                    usersService.creatUidAndRid(userandRoles);

                }
            }

        }catch (Exception e){
            e.getMessage();
            e.printStackTrace();
            return new ResponseCode(300,"用户更新失败",null,null);
        }


        return new ResponseCode(200,"用户更新成功",null,null);

    }

    @RequestMapping(value = "insert", method = RequestMethod.GET )
    public String  userinsert( HttpServletRequest request,Model model, HttpServletResponse response) throws ServletException, IOException {
        List<Roles> roles = roleService.rolesList();

        model.addAttribute("roleslist",roles);
        return "users/insert";

    }

    @RequestMapping(value = "update", method = RequestMethod.POST )
    @ResponseBody
    public  ResponseCode updateonePOST( UsersModel usersModel,Model model,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Users usersone = usersService.userFindOne(usersModel.getId());

        if(usersone == null ){
            new ResponseCode(300,"数据修改失败",usersModel,"");
        }

        if(usersone.getId() <= 0){
            new ResponseCode(300,"数据修改失败",usersModel,"");
        }
        try {
           /* String password = usersModel.getPassword();
            String username = usersModel.getUsername();*/
            String roleName = "";
            List<Integer> roleIds = new ArrayList<>();
            String rolelist = usersModel.getRolelist();
            Object[] objectArray4Json = JsonUtils.getObjectArray4Json(rolelist);
            if(objectArray4Json.length > 0) {
                for (Object o : objectArray4Json) {

                    String[] split = o.toString().split(",");
                    roleIds.add(Integer.decode(split[0]));
                    roleName += split[1] + " ";

                }
            }
/*

            MDFive mdFive = new MDFive(password, username);
*/

           /* usersone.setUsername(usersModel.getUsername());*/
            usersone.setAddress(usersModel.getAddress());
            usersone.setEmail(usersModel.getEmail());
            usersone.setRoleName(roleName);
            usersone.setMobile(usersModel.getMobile());
            usersone.setIsdelete(0);
          /*  usersone.setPassword(mdFive.getPassword());*/
          /*  usersone.setSalt(mdFive.getSalt());*/
            usersone.setRealName(usersModel.getRealName());
            usersService.update(usersone);

            usersService.roleUpdates(usersModel.getId(),roleIds);


        }catch (Exception e){
            e.getMessage();
            return new ResponseCode(300,"用户更新失败",null,null);
        }

        String defaultUrl = cwmUtils.getAdminRefererCookie();

        return  new ResponseCode(200,"数据修改成功",usersModel,defaultUrl);

    }




    @RequestMapping(value = "update", method = RequestMethod.GET )
    public  String updateone( @RequestParam(defaultValue = "0") Integer id,Model model,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Roles> roles = roleService.rolesList();
        List<Integer> uidfinall = userAndroleMapper.uidfinall(id);

        List<SpinnerModel> spinnerModels = new ArrayList<>();

        for(Roles r :roles){
            SpinnerModel spinnerModel = new SpinnerModel();
            Integer id1 = r.getId();
            spinnerModel.setDescription(r.getDescription());
            spinnerModel.setRidanddescription(id1+","+r.getDescription());
            for(Integer rid :uidfinall){
                if(id1 == rid){
                    spinnerModel.setRoleboolean(true);
                }
            }

            spinnerModels.add(spinnerModel);
        }


        Users users = usersService.userFindOne(id);
        String defaultUrl = cwmUtils.getAdminRefererCookie();

        model.addAttribute("users",users);
        model.addAttribute("spinnerModels",spinnerModels);
        model.addAttribute("defaultUrl",defaultUrl);
        return  "users/update";

    }

    @RequestMapping(value = "delete", method = RequestMethod.GET )
    @ResponseBody
    public  ResponseCode delete( @RequestParam(defaultValue = "0") Integer id,Model model,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     try {
         Users users = usersService.userFindOne(id);
         usersService.delete(users);
     }catch (Exception e){
         e.getMessage();
         return new ResponseCode(300,"删除失败",null,null);
     }



        return  new ResponseCode(200,"删除成功",null,null);

    }






}
