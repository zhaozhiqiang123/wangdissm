package com.wangdi.ssm.domain.users;

public class Permissions {

    private Integer id;
    /*
    * 权限
    * */
    private String permission;
    /*
    * 权限描述
    * */
   private String description;


   private String available;
    private Integer isdelete=0;

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAvailable(String available) {
        this.available = available;
    }



    public String getPermission() {
        return permission;
    }

    public String getDescription() {
        return description;
    }

    public String getAvailable() {
        return available;
    }
}
