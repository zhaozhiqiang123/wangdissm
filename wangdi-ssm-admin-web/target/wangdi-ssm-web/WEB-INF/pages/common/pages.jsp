<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row">
    <!-- 分页码 -->
    <div class="col-md-12">
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li class="btn btn-rounded btn-outline-secondary">
                    <a href="${pageurl}">首页</a>
                </li>
                <!-- 如果还有前页就访问当前页码-1的页面， -->
                <c:if test="${pageInfo.hasPreviousPage}">
                    <li class="btn btn-rounded btn-outline-secondary">
                        <a href="${pageurl}?pageNumber=${pageInfo.pageNum-1}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                </c:if>
                <!--遍历所有导航页码，如果遍历的页码页当前页码相等就高亮显示，如果相等就普通显示  -->
                <c:forEach items="${pageInfo.navigatepageNums }" var="page_Nums">
                    <c:if test="${page_Nums==pageInfo.pageNum }">
                        <a class="btn btn-rounded btn-outline-secondary active" href="${pageurl}?pageNumber=${page_Nums}">${page_Nums}</a>
                    </c:if>
                    <c:if test="${page_Nums!=pageInfo.pageNum }">
                        <a class="btn btn-rounded btn-outline-secondary" href="${pageurl}?pageNumber=${page_Nums}">${page_Nums}</a>
                    </c:if>
                </c:forEach>
                <!-- 如果还有后页就访问当前页码+1的页面， -->
                <c:if test="${pageInfo.hasNextPage}">
                    <li class="btn btn-rounded btn-outline-secondary">
                        <a href="${pageurl}?pageNumber=${pageInfo.pageNum+1}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </c:if>
                <li class="btn btn-rounded btn-outline-secondary">
                    <a href="${pageurl}?pageNumber=${pageInfo.pages}">末页</a>
                </li>
            </ul>
        </nav>
        <p>当前第：${pageInfo.pageNum} 页，共 ${pageInfo.pages } 页，总共 ${pageInfo.total } 条记录</p>
    </div>
</div>
