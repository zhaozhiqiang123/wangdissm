package com.wangdi.ssm.mapper;

import com.github.pagehelper.Page;
import com.wangdi.ssm.domain.users.Roles;
import com.wangdi.ssm.domain.users.Users;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface RoleMapper {
    @Select("select * from roles where isdelete=0")
    List<Roles> roleList();

    void ridAnduidAdd(Integer uid, Integer rid);
}
