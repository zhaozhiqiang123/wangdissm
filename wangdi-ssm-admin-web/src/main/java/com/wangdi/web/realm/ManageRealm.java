package com.wangdi.web.realm;

import com.wangdi.service.users.UsersService;
import com.wangdi.ssm.domain.users.Users;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 想象成service
 * @author Administrator
 *
 */
public class ManageRealm extends AuthorizingRealm{
     @Autowired
	public UsersService usersService;
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		return null;
	}
	//认证
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

		String username = (String)authenticationToken.getPrincipal();
		Users user =null ;
		if("wang".equals(username)){
			user = new Users();
			user.setId(1);
			user.setPassword("6f23da2ff9fdb7d6659288292634b741");
			user.setUsername("wang");
			user.setSalt("9a44cd2b1c2d391ee64bc1bf4abfd31e");
		}else {
			List<Users> users = usersService.findByUsername(username);
			 user = users.get(0);
		}

		if(user == null) {
			throw new UnknownAccountException();//没找到帐号
		}
		if(Boolean.TRUE.equals(user.getIsdelete())) {
			throw new LockedAccountException(); //帐号锁定
		}
//交给 AuthenticatingRealm 使用 CredentialsMatcher 进行密码匹配，如果觉得人家 的不好可以在此判断或自定义实现

		SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
				user.getUsername(), //用户名
				user.getPassword(), //密码
				ByteSource.Util.bytes(user.getUsername() + user.getSalt()),//salt=username+salt
				getName() //realm name
		);
		return authenticationInfo;
	}

}
