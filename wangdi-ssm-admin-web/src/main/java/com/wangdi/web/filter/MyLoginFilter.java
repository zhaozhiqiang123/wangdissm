package com.wangdi.web.filter;

import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class MyLoginFilter extends FormAuthenticationFilter {
	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		HttpServletRequest req = (HttpServletRequest)request;
		
		String uri = req.getRequestURI();
		if(uri.equals("/login")){
			HttpSession session = req.getSession();
			Object validateCode = session.getAttribute("validateCode");
			String validateCodeParam = req.getParameter("validateCode");
			if(validateCode.toString().equals(validateCodeParam)){
				return super.onAccessDenied(request, response);
			}else{
				req.getRequestDispatcher("validateError").forward(request, response);
				return false;
			}
		}else{
			return super.onAccessDenied(request, response);
		}
	}
}
