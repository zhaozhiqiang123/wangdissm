<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <%@include file="../../common/head.jsp" %>

    <%--多选框必须依赖的--%>
    <link rel="stylesheet" href="/bootstrap-3.3.7-dist/css/bootstrap.css" type="text/css"/>
    <%--<script type="text/javascript" src="/static/jquery/js/jquery-3.5.1.min.js"></script>--%>
    <%@include file="../../common/js.jsp" %>
    <script type="text/javascript" src="/bootstrap-3.3.7-dist/js/bootstrap.js"></script>

    <!-- Include the plugin's CSS and JS: -->
    <!-- <script type="text/javascript" src="./dist/js/bootstrap-multiselect.js"></script> -->
    <!--无选择提示为中文,只是部分中文 -->
    <script type="text/javascript" src="/bootstrap-3.3.7-dist/dist/js/bootstrap-multiselect.min.js"></script>
    <link rel="stylesheet" href="/bootstrap-3.3.7-dist/dist/css/bootstrap-multiselect.css" type="text/css"/>
    <%--多选框必须依赖的--%>

    <script type="text/javascript">
        console.log("start");
        $(document).ready(function() {
            $('#example-multiple').multiselect();
            $('#example-radio').multiselect();
            $('#example-multiple-optgroups').multiselect();
            $('#example-radio-optgroups').multiselect();
            /*
            *分组可选，可折叠，全选时不显示，所有option的数量
            */
            $('#example-enableClickableOptGroups').multiselect({
                enableClickableOptGroups: true,
                selectAllNumber: false
            });
            /*
            *分组可选，可折叠，可全选，右侧显示下拉选项，
            *触发选择事件，可搜索，可展示已选择option的数量（numberDisplayed）
            */
            $('#example-all').multiselect({
                enableClickableOptGroups: true,
                enableCollapsibleOptGroups: true,
                includeSelectAllOption: true,
                buttonWidth: '400px',
                dropRight: true,
                maxHeight: 200,
                onChange: function(option, checked) {
                    alert($(option).val());
                    /*if(条件) {
          this.clearSelection();//清除所有选择及显示
     }*/
                },
                nonSelectedText: '请选择',
                numberDisplayed: 10,
                enableFiltering: true,
                allSelectedText:'全部',
            });

        });
    </script>
</head>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    <%@include file="../../common/header.jsp" %>

    <div class="main-container">

        <%@include file="../../common/sidebar.jsp" %>

        <div class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header text-white bg-primary">
                            新增客户
                        </div>

                        <div class="card-body">
                            <label>用户名</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                </div>
                                <input type="text" name="username" class="form-control" placeholder="username">
                            </div>

                            <label>真实姓名</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input type="text" name="realName" class="form-control" placeholder="realName">
                            </div>

                            <label>邮箱</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input type="email" name="email" class="form-control" placeholder="Email">
                            </div>
                            <label>电话</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input type="text"  onkeyup="value=value.replace(/\D+/g,'')"
                                       name="mobile" class="form-control" placeholder="mobile">
                            </div>
                            <label>密码</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input type="text" name="password" class="form-control" placeholder="password">
                            </div>



                            <label>角色表</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user-friends"></i></span>
                                </div>
                            <select id="example-multiple"  name="rolelist" multiple="multiple">


                                <c:forEach items="${roleslist}" var="roles">
                                    <option value="${roles.id },${roles.description}" >${roles.description}</option>
                                </c:forEach>
                            </select>
                            </div>

                            <label>详细地址</label>
                            <div class="input-group mb-3">
                                <textarea id="textarea" name="address" class="form-control" rows="6" placeholder="Detailed address"></textarea>
                            </div>

                            <button type="button" class="btn btn-block btn-info" onclick="insertCustomer()">新增客户</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="../../common/message.jsp" %>

<script>
    function insertCustomer() {
        var username = $("input[name='username']").val();
        var email = $("input[name='email']").val();
        var address = $("#textarea").val();
        var rolelist = $("select[name='rolelist']").val();

        var realName = $("input[name='realName']").val();
        var mobile = $("input[name='mobile']").val();
        var password = $("input[name='password']").val();

       if (username == null || username.length == 0) {
            $('#modal-danger').modal('show');
            $('#modal-danger .modal-body').html('姓名不能为空');
            return;
        }

        if (password == null || password.length < 7) {
            $('#modal-danger').modal('show');
            $('#modal-danger .modal-body').html('密码不能为空并且不能小于8位');
            return;
        }
        if (email == null || email.length == 0) {
            $('#modal-danger').modal('show');
            $('#modal-danger .modal-body').html('邮箱不能为空');
            return;
        }
        if (mobile == null || mobile.length > 11 || mobile.length == 0) {
            $('#modal-danger').modal('show');
            $('#modal-danger .modal-body').html('联系电话不能为空');
            return;
        }
        if (address == null || address.length == 0) {
            $('#modal-danger').modal('show');
            $('#modal-danger .modal-body').html('客户详细地址不能为空');
            return;
        }

        $.ajax({
            type: 'POST',
            url: "/users/userinsert",
            data: {
                username: username,
                email: email,
                address: address,
                mobile: mobile,
                realName: realName,
                password: password,
                rolelist:JSON.stringify(rolelist)
            },
            dataType: "json",
            success: function(result) {
                console.log("成功方法1");
                console.log(result);
                if (result.code == 200) {
                    console.log("成功方法");
                    $('#modal-success').modal('show');
                    $('#modal-success .modal-body').html(result.msg);
                    $("#successBtn").on("click", function () {
                        window.location.href = "/users/insert";
                    });
                } else {
                    console.log("300方法");
                    $('#modal-danger').modal('show');
                    $('#modal-danger .modal-body').html("失败：状态码：" + result.code + "，" + result.msg);
                }
            },
            error: function () {
                console.log("error方法");
                $('#modal-danger').modal('show');
                $('#modal-danger .modal-body').html('请求失败，请检查请求数据或网络哟');
            }
        });
    }





</script>
</body>
</html>
